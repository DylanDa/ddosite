const paths = require("./paths");

const { merge } = require("webpack-merge");
const common = require("./webpack.common.js");

const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

module.exports = merge(common, {
  // Set the mode to development or production
  mode: "development",

  // Control how source maps are generated
  devtool: "inline-source-map",

  devServer: {
    host: "0.0.0.0",
    historyApiFallback: true,
    contentBase: paths.build,
    open: true,
    openPage: "http://localhost:8080",
    compress: true,
    hot: false,
    liveReload: true,
    disableHostCheck: true,
    watchContentBase: true,
    port: 8080,
  },

  module: {
    rules: [
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: "style-loader",
          },
          {
            loader: "css-loader",
            options: {
              sourceMap: true,
            },
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true,
            },
          },
          {
            loader: "postcss-loader",
            options: {
              ident: "postcss",
              plugins: [require("tailwindcss"), require("autoprefixer")],
            },
          },
        ],
      },
    ],
  },

  //plugins: [new BundleAnalyzerPlugin()],
});
