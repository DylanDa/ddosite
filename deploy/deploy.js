const server = require("./config");

const scpClient = require("scp2");

var Client = require("ssh2").Client;

var conn = new Client();
conn
  .on("ready", function () {
    console.log("Client :: ready");
    conn.exec(`rm -rf ${server.path}/*`, function (err, stream) {
      if (err) throw err;
      stream
        .on("close", function (code, signal) {
          console.log("Stream :: close :: code: " + code + ", signal: " + signal);

          scpClient.scp(
            "dist/",
            {
              host: server.host,
              port: server.port,
              username: server.username,
              password: server.password,
              path: server.path,
            },
            function (err) {
              if (err) {
                console.log("Publishing failure.\n");
                throw err;
              } else {
                console.log("Success! Successfully published to PI server! \n");
              }
            }
          );

          conn.end();
        })
        .on("data", function (data) {
          console.log("STDOUT: " + data);
        })
        .stderr.on("data", function (data) {
          console.log("STDERR: " + data);
        });
    });
  })
  .connect({
    host: server.host,
    port: server.port,
    username: server.username,
    password: server.password,
  });
