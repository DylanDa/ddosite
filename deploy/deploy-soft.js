const scpClient = require("scp2");

const server = require("./config");

scpClient.scp(
  "dist/",
  {
    host: server.host,
    port: server.port,
    username: server.username,
    password: server.password,
    path: server.path,
  },
  function (err) {
    if (err) {
      console.log("Publishing failure.\n");
      throw err;
    } else {
      console.log("Success! Successfully published to PI server! \n");
    }
  }
);
