/**
 * Server related configuration
 */
const SERVER_LIST = {
  id: "test",
  name: "testing environment",
  host: "", // ip
  port: 22, // port
  username: "",
  password: "",
  path: "/var/www/html/", // Project static file storage address
};
module.exports = SERVER_LIST;
