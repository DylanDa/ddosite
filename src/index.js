import css from "./css/main.scss";

import MainThreeJS from "./js/main3js";

import * as THREE from "three";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

import { theme } from "../tailwind.config.js";

import LocomotiveScroll from "locomotive-scroll";

class Application {
  constructor() {
    this.mouseOffset = {};
    this.mousePos = {};
    this.tailwindConf = theme;
    this.currentTheme = "dark";
    this.currentScene = "home";

    this.discoverScenes = {};
    this.animations = {
      home: [],
      about: [],
    };

    this._init();
  }

  _init() {
    this._initAppLoader();

    this._initCursor();
    this._initMouseEvents();
    this._initLocoScroll();
    this._initNav();
  }

  initAfterLoad() {
    document.querySelector(".app-loader").remove(); //Remove apploader

    this._initDiscoverScenes();

    this._initCursorAnimations();

    this._initEntryAnimations();

    this._initHomeDOMAnimations();

    //More about me button hover
    let navAbout = document.querySelector("#nav-about");

    //Remove hover effect on mobile after click
    navAbout.addEventListener("click", (e) => {
      if (this.mainThreeJS.mobile) {
        var event = new Event("mouseleave", {
          bubbles: true,
          cancelable: true,
        });
        navAbout.dispatchEvent(event);
      }
    });

    let navAboutHover = gsap.to(navAbout, { backgroundColor: "transparent", color: "#fff", duration: 0, paused: true, ease: "ease.out" });
    navAbout.addEventListener("mouseenter", () => navAboutHover.play());
    navAbout.addEventListener("mouseleave", () => {
      navAboutHover.reverse();
    });
    navAbout.addEventListener("mouseup", () => navAboutHover.reverse());

    //Mail clipboard
    let messageCopy = "Click to copy";
    let messageSuccess = "Email address copied to clipboard";
    let mailLinks = document.getElementsByClassName("mailcp-link");
    Array.from(mailLinks).forEach((link) => {
      let msgSpan = document.createElement("span");
      msgSpan.classList.add("mailcp-message");
      msgSpan.innerHTML = messageCopy;
      link.querySelector(".mail-holder").append(msgSpan);
      link.addEventListener("click", (e) => {
        e.preventDefault();
        this.copyToClipboard(e.target.innerHTML);
        this.mailCopiedOnce = true;
        msgSpan.innerHTML = messageSuccess;
        setTimeout(function () {
          msgSpan.innerHTML = messageCopy;
        }, 2000);
      });
    });

    //Contact page
    // Calculate letters path length
    const letters = ["d", "a", "c", "o", "n", "c2", "e", "i-b", "i-p", "c3", "a2", "o2"];
    letters.forEach((letter, index, el) => {
      const clazz = `.letter-${letter}`; // Prepend #mask- to each mask element name
      let paths = document.querySelectorAll(clazz);
      Array.from(paths).forEach((path) => {
        const length = path.getTotalLength(); // Calculate the length of a path
        path.style.strokeDasharray = length; // Set the length to stroke-dasharray in the styles
        path.style.strokeDashoffset = length; // Set the length to stroke-dashoffset in the styles
      });
    });
    //Home contact
    let contactHomeActionTl = gsap.timeline({ paused: true });
    contactHomeActionTl.set(".contact-home .rotterdam-img-wrapper", {
      zIndex: 15,
    });
    contactHomeActionTl.set(".contact-home .metz-img-wrapper", {
      zIndex: 10,
    });
    contactHomeActionTl.to(".contact-home .metz-img-wrapper", {
      x: 30,
      y: -30,
      opacity: 0.05,
      duration: 0.5,
      ease: "power2.inOut",
    });
    contactHomeActionTl.to(
      ".contact-home .rotterdam-img-wrapper",
      {
        x: 0,
        y: 0,
        opacity: 1,
        duration: 0.5,
        ease: "power2.inOut",
      },
      "<"
    );
    // let contactHomeShakeTl = gsap.timeline({ paused: true });
    // contactHomeShakeTl.from(".contact-home .rotterdam-img-wrapper", {
    //   x: -5,
    //   y: -5,
    //   duration: 1,
    //   ease: "elastic.inOut",
    // });

    Array.from(document.querySelectorAll(".contact-home .contact-action")).forEach((link) => {
      link.addEventListener("mouseenter", (event) => {
        contactHomeActionTl.play();
      });
      link.addEventListener("mouseleave", (event) => {
        if (!this.mailCopiedOnce) {
          contactHomeActionTl.reverse();
        }
        //  else {
        //   contactHomeShakeTl.invalidate().restart();
        // }
      });
    });
    //About contact
    let contactAboutActionTl = gsap.timeline({ paused: true });
    contactAboutActionTl.set(".contact-about .rotterdam-img-wrapper", {
      zIndex: 15,
    });
    contactAboutActionTl.set(".contact-about .metz-img-wrapper", {
      zIndex: 10,
    });
    contactAboutActionTl.to(".contact-about .metz-img-wrapper", {
      x: -30,
      y: -30,
      opacity: 0.05,
      duration: 0.5,
      ease: "power2.inOut",
    });
    contactAboutActionTl.to(
      ".contact-about .rotterdam-img-wrapper",
      {
        x: 0,
        y: 0,
        opacity: 1,
        duration: 0.5,
        ease: "power2.inOut",
      },
      "<"
    );
    Array.from(document.querySelectorAll(".contact-about .contact-action")).forEach((link) => {
      link.addEventListener("mouseenter", (event) => {
        contactAboutActionTl.play();
      });
      link.addEventListener("mouseleave", (event) => {
        if (!this.mailCopiedOnce) {
          contactAboutActionTl.reverse();
        }
      });
    });

    //Aboutme img follow
    var aboutmeIntro = document.querySelector(".aboutme");
    var aboutmeIntroTexts = document.querySelectorAll(".aboutme .intro-item");
    var meImg = document.querySelector("#aboutme-img");
    var meImgTl = gsap.timeline({ paused: true });
    meImgTl.to(meImg, {
      visibility: "visible",
      opacity: 1,
      duration: 1,
      ease: "power2.inOut",
    });
    aboutmeIntro.addEventListener("mouseenter", (e) => {
      if (!this.mainThreeJS.mobile) {
        this.showAboutmeImg = true;
        meImg.style.display = "block";
        meImgTl.timeScale(1).play();
      }
    });
    aboutmeIntro.addEventListener("mouseleave", (e) => {
      if (!this.mainThreeJS.mobile) {
        this.showAboutmeImg = false;
        meImgTl.timeScale(3).reverse();
      }
    });

    aboutmeIntroTexts.forEach((box) =>
      box.addEventListener("mouseenter", (e) => {
        meImg.style.filter = "grayscale(0)";
      })
    );

    aboutmeIntroTexts.forEach((box) =>
      box.addEventListener("mouseleave", (e) => {
        meImg.style.filter = "grayscale(1)";
      })
    );

    var setAboutmeImgX = gsap.quickSetter(meImg, "x", "px");
    var setAboutmeImgY = gsap.quickSetter(meImg, "y", "px");
    var pos = { x: this.mousePos.x, y: this.mousePos.y };
    var speed = 0.1;
    var fpms = 60 / 1000;

    //Pointer circle animate
    gsap.ticker.add((time, deltaTime) => {
      if (this.showAboutmeImg) {
        var delta = deltaTime * fpms;
        var dt = 1.0 - Math.pow(1.0 - speed, delta);

        pos.x += (this.mousePos.x - (pos.x + meImg.width / 2)) * dt;
        pos.y += (this.mousePos.y - (pos.y + meImg.height - 100)) * dt;
        setAboutmeImgX(pos.x);
        setAboutmeImgY(pos.y);
      }
    });

    // each time the window updates, we should refresh ScrollTrigger and then update LocomotiveScroll.
    ScrollTrigger.addEventListener("refresh", () => {
      this.locoScroll.update();
    });

    // after everything is set up, refresh() ScrollTrigger and update LocomotiveScroll because padding may have been added for pinning, etc.
    ScrollTrigger.refresh();

    //SCRIPT END
  }

  _initAppLoader() {
    //Page loading
    const LOADER_RADIUS = 100;
    this.loaderCircumference = 2 * Math.PI * LOADER_RADIUS;
    this.loaderPath = document.querySelector(".loader__path");
    this.loaderPercentage = document.querySelector(".loader__percentage");
  }

  _initLocoScroll() {
    this.locoScroll = new LocomotiveScroll({
      el: document.querySelector(".smooth-scroll"),
      smooth: true,
      lerp: 0.1,
      touchMultiplier: 3,
      smartphone: {
        smooth: true,
      },
      tablet: {
        smooth: true,
      },
    });

    // each time Locomotive Scroll updates, tell ScrollTrigger to update too (sync positioning)
    this.locoScroll.on("scroll", ScrollTrigger.update);

    // tell ScrollTrigger to use these proxy methods for the ".smooth-scroll" element since Locomotive Scroll is hijacking things
    ScrollTrigger.scrollerProxy(".smooth-scroll", {
      scrollTop: (value) => {
        return arguments.length ? this.locoScroll.scrollTo(value, 0, 0) : this.locoScroll.scroll.instance.scroll.y;
      }, // we don't have to define a scrollLeft because we're only scrolling vertically.
      getBoundingClientRect() {
        return { top: 0, left: 0, width: window.innerWidth, height: window.innerHeight };
      },
      // LocomotiveScroll handles things completely differently on mobile devices - it doesn't even transform the container at all! So to get the correct behavior and avoid jitters, we should pin things with position: fixed on mobile. We sense it by checking to see if there's a transform applied to the container (the LocomotiveScroll-controlled element).
      pinType: document.querySelector(".smooth-scroll").style.transform ? "transform" : "fixed",
    });
  }

  _initCursor() {
    this.cursorCircle = document.getElementById("cursor-circle");
    this.cursorPointer = document.getElementById("cursor-pointer");
    this.cursorPointerDot = this.cursorPointer.querySelector("circle");
    gsap.set(this.cursorCircle, { xPercent: -50, yPercent: -50 });
    gsap.set(this.cursorPointer, { xPercent: -50, yPercent: -50 });
    var pos = { x: -50, y: -50 };
    this.mousePos = { x: pos.x, y: pos.y };
    var speed = 0.2;

    var fpms = 60 / 1000;

    this._setPointerCirclePosX = gsap.quickSetter(this.cursorCircle, "x", "px");
    this._setPointerCirclePosY = gsap.quickSetter(this.cursorCircle, "y", "px");
    this._setPointerDotPosX = gsap.quickSetter(this.cursorPointer, "x", "px");
    this._setPointerDotPosY = gsap.quickSetter(this.cursorPointer, "y", "px");

    //Init outside of screen
    this._setPointerDotPosX(pos.x);
    this._setPointerDotPosY(pos.y);

    //Pointer circle animate
    gsap.ticker.add((time, deltaTime) => {
      var delta = deltaTime * fpms;
      var dt = 1.0 - Math.pow(1.0 - speed, delta);

      pos.x += (this.mousePos.x - pos.x) * dt;
      pos.y += (this.mousePos.y - pos.y) * dt;
      this._setPointerCirclePosX(pos.x);
      this._setPointerCirclePosY(pos.y);
    });
  }

  _initMouseEvents() {
    window.addEventListener("mousemove", (event) => {
      //Mouse X/Y -1/1
      this.mouseOffset.x = (event.clientX - window.innerWidth / 2) / (window.innerWidth / 2);
      this.mouseOffset.y = (event.clientY - window.innerHeight / 2) / (window.innerHeight / 2);

      //Mouse X/Y
      this.mousePos.x = event.x;
      this.mousePos.y = event.y;

      //Set pointer dot position
      this._setPointerDotPosX(event.x);
      this._setPointerDotPosY(event.y);
    });
  }

  _initNav() {
    //Change scene buttons
    let aboutButtons = document.getElementsByClassName("about-scene-button");
    for (let i = 0; i < aboutButtons.length; i++) {
      aboutButtons[i].addEventListener("click", (e) => {
        e.preventDefault();
        this.goToAboutScene();
      });
    }

    let homeButtons = document.getElementsByClassName("home-scene-button");
    for (let i = 0; i < homeButtons.length; i++) {
      homeButtons[i].addEventListener("click", (e) => {
        e.preventDefault();
        this.goToHomeScene();
      });
    }
  }

  _initCursorAnimations() {
    //Hover animation on cursor
    var pointerEnterLinkToSceneTl = gsap.timeline({ paused: true });
    pointerEnterLinkToSceneTl.to(this.cursorPointer, {
      scale: 3.5,
      duration: 0.5,
      ease: "power2.out",
    });
    pointerEnterLinkToSceneTl.to(
      this.cursorCircle,
      {
        scale: 1.5,
        opacity: 0,
        duration: 0.2,
        ease: "power2.out",
      },
      "<=0"
    );
    pointerEnterLinkToSceneTl.to(
      this.mainThreeJS.cursorLight,
      {
        intensity: 2.5,
        duration: 0.4,
        ease: "power2.in",
      },
      "<=0"
    );
    pointerEnterLinkToSceneTl.to(
      this.mainThreeJS.secondLight,
      {
        intensity: 8,
        duration: 0.4,
        ease: "power2.in",
      },
      "<=0"
    );
    pointerEnterLinkToSceneTl.to(
      this.mainThreeJS.secondLight.position,
      {
        z: 100,
        //duration: 0.4,
        //ease: "power2.in",
      },
      "<=0"
    );
    pointerEnterLinkToSceneTl.to(
      this.mainThreeJS.light1,
      {
        intensity: 2,
        duration: 0.4,
        ease: "power2.in",
      },
      "<=0"
    );
    pointerEnterLinkToSceneTl.to(
      this.mainThreeJS.light2,
      {
        intensity: 2,
        duration: 0.4,
        ease: "power2.in",
      },
      "<=0"
    );
    pointerEnterLinkToSceneTl.to(
      this.mainThreeJS.light3,
      {
        intensity: 2,
        duration: 0.4,
        ease: "power2.in",
      },
      "<=0"
    );
    pointerEnterLinkToSceneTl.to(
      this.mainThreeJS.object.material,
      {
        roughness: 0.23,
        metalness: 0,
        ease: "power2.in",
      },
      "<=0"
    );
    var pointerEnterLinkTl = gsap.timeline({ paused: true });
    pointerEnterLinkTl.to(this.cursorPointer, {
      scale: 3.5,
      duration: 0.5,
      ease: "power2.out",
    });
    pointerEnterLinkTl.to(
      this.cursorCircle,
      {
        scale: 1.5,
        opacity: 0,
        duration: 0.2,
        ease: "power2.out",
      },
      "<=0"
    );
    pointerEnterLinkTl.to(
      this.mainThreeJS.cursorLight,
      {
        intensity: 2.5,
        duration: 0.4,
        ease: "power2.in",
      },
      "<=0"
    );
    pointerEnterLinkTl.to(
      this.mainThreeJS.secondLight,
      {
        intensity: 16,
        duration: 0.4,
        ease: "power2.in",
      },
      "<=0"
    );
    pointerEnterLinkTl.to(
      this.mainThreeJS.light1,
      {
        intensity: 2,
        duration: 0.4,
        ease: "power2.in",
      },
      "<=0"
    );
    pointerEnterLinkTl.to(
      this.mainThreeJS.light2,
      {
        intensity: 2,
        duration: 0.4,
        ease: "power2.in",
      },
      "<=0"
    );
    pointerEnterLinkTl.to(
      this.mainThreeJS.light3,
      {
        intensity: 2,
        duration: 0.4,
        ease: "power2.in",
      },
      "<=0"
    );

    document.querySelectorAll(".p-action-click").forEach((actionElem) => {
      actionElem.addEventListener("mouseenter", function (event) {
        pointerEnterLinkTl.timeScale(1).play();
      });
    });
    document.querySelectorAll(".p-action-click").forEach((actionElem) => {
      actionElem.addEventListener("mouseleave", function (event) {
        pointerEnterLinkTl.timeScale(2).reverse();
      });
    });
    document.querySelectorAll(".p-scene-click").forEach((actionElem) => {
      actionElem.addEventListener("mouseenter", (event) => {
        if (this.entryAnimationDone) pointerEnterLinkToSceneTl.timeScale(1).play();
      });
    });
    document.querySelectorAll(".p-scene-click").forEach((actionElem) => {
      actionElem.addEventListener("mouseleave", (event) => {
        if (this.entryAnimationDone) pointerEnterLinkToSceneTl.timeScale(2).reverse();
      });
    });

    //Scroll down link
    document.getElementById("scroll-down-link").addEventListener("click", (e) => {
      e.preventDefault();
      this.locoScroll.scrollTo(document.querySelector(".experiences"));
    });
    document.getElementById("scroll-down-link-aboutme").addEventListener("click", (e) => {
      e.preventDefault();
      this.locoScroll.scrollTo(document.querySelector(".studies"));
    });

    //Pointer up/down animation
    var pointerDownTl = gsap.timeline({ paused: true });
    pointerDownTl.to(this.cursorPointer, {
      scale: 2,
      duration: 0.5,
      ease: "power2.out",
    });
    pointerDownTl.to(
      this.cursorCircle,
      {
        scale: 1.5,
        opacity: 0,
        duration: 0.2,
        ease: "power2.out",
      },
      "<=0"
    );
    var pointerUpTl = gsap.timeline({ paused: true });
    pointerUpTl.to(this.cursorPointer, {
      scale: 1,
      duration: 0.2,
      ease: "power2.out",
    });
    pointerUpTl.to(
      this.cursorCircle,
      {
        scale: 1,
        opacity: 1,
        duration: 0.5,
        ease: "power2.out",
      },
      "<=0"
    );
    window.addEventListener("mousedown", function (event) {
      pointerUpTl.pause();
      pointerDownTl.invalidate().restart();
    });
    window.addEventListener("mouseup", function (event) {
      pointerDownTl.pause();
      pointerUpTl.invalidate().restart();
    });
  }

  _initDiscoverScenes() {
    //Discover buttons
    let discoverButtons = document.getElementsByClassName("discover-action");
    for (let i = 0; i < discoverButtons.length; i++) {
      this.createOrGetDiscoverScene(discoverButtons[i].dataset.id);
      discoverButtons[i].addEventListener("click", (e) => {
        e.preventDefault();

        this.mainThreeJS.triggerParticlesAccel();

        this.locoScroll.stop();
        document.getElementById("nav-about").style.display = "none";

        this.discoverActive = discoverButtons[i].dataset.id;

        this.createOrGetDiscoverScene(discoverButtons[i].dataset.id).play();
      });
    }
    let discoverLeaveButtons = document.getElementsByClassName("discover-close-action");
    for (let i = 0; i < discoverLeaveButtons.length; i++) {
      let discoverParentWrapper = discoverLeaveButtons[i].closest(".discover-wrapper");
      discoverLeaveButtons[i].addEventListener("click", (e) => {
        e.preventDefault();

        this.mainThreeJS.triggerParticlesAccel();

        this.discoverActive = false;

        this.createOrGetDiscoverScene(discoverParentWrapper.dataset.id).reverse();
        this.locoScroll.start();
      });
    }
  }

  _initEntryAnimations() {
    //ThreeJS entry animation
    var tl = gsap.timeline({
      onComplete: () => {
        gsap.fromTo(
          this.mainThreeJS.object.position,
          {
            x: this.mainThreeJS.modelConf.right.regular.pos.x,
            y: this.mainThreeJS.modelConf.right.regular.pos.y,
          },
          {
            x: this.mainThreeJS.modelConf.right.low.pos.x,
            y: this.mainThreeJS.modelConf.right.low.pos.y,
            scrollTrigger: {
              trigger: ".contact-home",
              scroller: ".smooth-scroll",
              start: "top center",
              end: "top top",
              toggleActions: "play pause resume reverse",
            },
          }
        );
        this.entryAnimationDone = true;
      },
    });
    tl.to(this.mainThreeJS.object.position, {
      x: this.mainThreeJS.modelConf.right.regular.pos.x,
      y: this.mainThreeJS.modelConf.right.regular.pos.y,
      z: this.mainThreeJS.modelConf.right.regular.pos.z,
      duration: 3,
      ease: "power4.out",
    });

    var tl2 = gsap.timeline();
    let colorInit = new THREE.Color(0x201010);
    tl2.from(this.mainThreeJS.object.material.color, {
      r: colorInit.r,
      g: colorInit.g,
      b: colorInit.b,
      duration: 2,
      ease: "elastic.in",
    });
    tl2.from(
      this.mainThreeJS.object.material,
      {
        metalness: 0.1,
        duration: 1,
        ease: "power2.inOut",
      },
      "<=0"
    );
    tl2.fromTo(
      ".ddo-backtext",
      {
        x: 300,
        y: 0,
      },
      {
        opacity: 0.2,
        x: 0,
        y: 0,
        duration: 2,
        ease: "power2.out",
        stagger: 0.4,
      },
      "<"
    );
    tl2.from(
      this.mainThreeJS.object.material,
      {
        wireframe: true,
        duration: 0.55,
      },
      "<2"
    );
    tl2.fromTo(
      this.mainThreeJS.secondLight.position,
      {
        z: 60,
      },
      {
        z: this.mainThreeJS.conf.secondLightZ,
        duration: 1,
        ease: "sine.out",
      },
      "<0"
    );

    gsap.fromTo(
      "header",
      {
        y: -100,
      },
      {
        y: 0,
        opacity: 1,
        duration: 1.5,
        ease: "power2.out",
      }
    );

    //Intro arrows down animation
    gsap.to(".intro-arrow-down .intro-arrow", {
      y: 10,
      stagger: 0.1,
      repeat: -1,
      yoyo: true,
      duration: 1,
      ease: "ease.in",
    });
    //Intro arrows down animation
    gsap.to(".intro-arrow-down-about .intro-arrow", {
      y: 10,
      stagger: 0.1,
      repeat: -1,
      yoyo: true,
      duration: 1,
      ease: "ease.in",
    });
  }

  _initHomeDOMAnimations() {
    this.animations.home.push(
      gsap.to(".intro-arrow-down .intro-arrow", {
        scrollTrigger: {
          trigger: ".intro",
          scroller: ".smooth-scroll",
          start: "top top",
          end: "top bottom",
          scrub: 1,
          toggleActions: "play pause resume reverse",
        },
        opacity: 0,
        stagger: 1,
        duration: 1,
        ease: "ease.in",
      })
    );

    //Sections animations
    this.animations.home.push(
      gsap.fromTo(
        ".intro",
        {
          y: 100,
        },
        {
          y: 0,
          opacity: 1,
          ease: "ease.in",
          duration: 1.5,
        }
      )
    );

    this.animations.home.push(
      gsap.fromTo(
        ".experiences",
        {
          opacity: 0,
          y: 40,
        },
        {
          scrollTrigger: {
            trigger: ".experiences",
            scroller: ".smooth-scroll",
            start: "top center",
            end: "top top",
            toggleActions: "play none none none",
          },
          opacity: 1,
          y: 0,
          ease: "ease.in",
          duration: 1,
        }
      )
    );
    this.animations.home.push(
      gsap.fromTo(
        ".contact-home",
        {
          opacity: 0,
          y: 40,
        },
        {
          scrollTrigger: {
            trigger: ".contact-home",
            scroller: ".smooth-scroll",
            start: "top center",
            end: "top top",
            toggleActions: "play none none none",
          },
          opacity: 1,
          y: 0,
          ease: "ease.in",
          duration: 1,
        }
      )
    );

    this.animations.home.push(
      gsap.fromTo(
        "#daconceicao-hw-home .letter",
        {
          opacity: 0,
        },
        {
          "stroke-dashoffset": 0,
          opacity: 1,
          scrollTrigger: {
            trigger: ".contact-home",
            scroller: ".smooth-scroll",
            start: "top center",
            end: "top top",
            toggleActions: "play none none none",
          },
          stagger: 0.2,
          duration: 0.3,
          ease: "ease.in",
        }
      )
    );

    this.animations.home.push(
      gsap.to("#nav-about", {
        backgroundColor: this.tailwindConf.colors.primary,
        scrollTrigger: {
          trigger: ".contact-home",
          scroller: ".smooth-scroll",
          start: "top center",
          end: "top top",
          scrub: true,
          toggleActions: "play pause resume reset",
        },
      })
    );
  }

  _initHomeThreeJSAnimations() {
    if (this.homeSceneTl) this.homeSceneTl.pause().kill();

    this.homeSceneTl = gsap.timeline({
      autoRemoveChildren: true,
      onComplete: () => {
        this.animations.home.push(
          gsap.fromTo(
            this.mainThreeJS.object.position,
            {
              x: this.mainThreeJS.modelConf.right.regular.pos.x,
              y: this.mainThreeJS.modelConf.right.regular.pos.y,
            },
            {
              x: this.mainThreeJS.modelConf.right.low.pos.x,
              y: this.mainThreeJS.modelConf.right.low.pos.y,
              scrollTrigger: {
                trigger: ".contact-home",
                scroller: ".smooth-scroll",
                start: "top center",
                end: "top top",
                toggleActions: "play pause resume reverse",
              },
            }
          )
        );
      },
    });
    this.homeSceneTl.to(this.mainThreeJS.object.position, {
      x: this.mainThreeJS.modelConf.right.regular.pos.x,
      y: this.mainThreeJS.modelConf.right.regular.pos.y,
      duration: 1,
      ease: "sine.out",
    });
    this.homeSceneTl.to(
      this.mainThreeJS.object.rotation,
      {
        y: THREE.Math.degToRad(this.mainThreeJS.modelConf.right.regular.rotation.y),
        duration: 1,
        ease: "power2.out",
      },
      "-=1"
    );

    this.animations.home.push(
      gsap.to(this.mainThreeJS.secondLight.position, {
        x: this.mainThreeJS.conf.secondLightX,
        duration: 4,
        ease: "ease.out",
      })
    );
    this.animations.home.push(
      gsap.to(this.mainThreeJS.mainLight.position, {
        x: this.mainThreeJS.conf.mainLightX,
        duration: 2,
        ease: "ease.out",
      })
    );
  }

  _initAboutDOMAnimations() {
    // Sections animations
    this.animations.about.push(
      gsap.to(".intro-arrow-down-about .intro-arrow", {
        scrollTrigger: {
          trigger: ".intro-aboutme",
          scroller: ".smooth-scroll",
          start: "top top",
          end: "top bottom",
          scrub: 1,
          toggleActions: "play pause resume reverse",
        },
        opacity: 0,
        stagger: 1,
        duration: 1,
        ease: "ease.in",
      })
    );
    this.animations.about.push(
      gsap.fromTo(
        ".aboutme",
        {
          y: 100,
        },
        {
          y: 0,
          opacity: 1,
          ease: "ease.in",
          duration: 1.5,
        }
      )
    );
    this.animations.about.push(
      gsap.fromTo(
        ".studies",
        {
          opacity: 0,
          y: 40,
        },
        {
          scrollTrigger: {
            trigger: ".studies",
            scroller: ".smooth-scroll",
            start: "top center",
            end: "top top",
          },
          opacity: 1,
          y: 0,
          ease: "ease.in",
          duration: 1,
        },
        "<=0"
      )
    );

    this.animations.about.push(
      gsap.fromTo(
        ".contact-about",
        {
          opacity: 0,
          y: 40,
        },
        {
          scrollTrigger: {
            trigger: ".contact-about",
            scroller: ".smooth-scroll",
            start: "top center",
            end: "top top",
            toggleActions: "play none none none",
          },
          opacity: 1,
          y: 0,
          ease: "ease.in",
          duration: 1,
        }
      )
    );

    this.animations.about.push(
      gsap.fromTo(
        "#daconceicao-hw-about .letter",
        {
          opacity: 0,
        },
        {
          "stroke-dashoffset": 0,
          opacity: 1,
          scrollTrigger: {
            trigger: ".contact-about",
            scroller: ".smooth-scroll",
            start: "top center",
            end: "top top",
            toggleActions: "play none none none",
          },
          stagger: 0.2,
          duration: 0.3,
          ease: "ease.in",
        }
      )
    );
  }

  _initAboutThreeJSAnimations() {
    if (this.aboutSceneTl) this.aboutSceneTl.pause().kill();

    this.aboutSceneTl = gsap.timeline({
      autoRemoveChildren: true,
      onComplete: () => {
        this.animations.about.push(
          gsap.fromTo(
            this.mainThreeJS.object.position,
            {
              x: this.mainThreeJS.modelConf.left.regular.pos.x,
              y: this.mainThreeJS.modelConf.left.regular.pos.y,
            },
            {
              x: this.mainThreeJS.modelConf.left.low.pos.x,
              y: this.mainThreeJS.modelConf.left.low.pos.y,
              scrollTrigger: {
                trigger: ".contact-about",
                scroller: ".smooth-scroll",
                start: "top center",
                end: "top top",
                toggleActions: "play pause resume reverse",
              },
            }
          )
        );
      },
    });

    this.aboutSceneTl.to(this.mainThreeJS.object.position, {
      x: this.mainThreeJS.modelConf.left.regular.pos.x,
      y: this.mainThreeJS.modelConf.left.regular.pos.y,
      duration: 3,
      ease: "power2.out",
    });
    this.aboutSceneTl.to(
      this.mainThreeJS.object.rotation,
      {
        y: THREE.Math.degToRad(this.mainThreeJS.modelConf.left.regular.rotation.y),
        duration: 3,
        ease: "expo.out",
      },
      ">-3"
    );

    this.animations.about.push(
      gsap.to(this.mainThreeJS.secondLight.position, {
        x: -3,
        duration: 4,
        ease: "ease.out",
      })
    );
    this.animations.about.push(
      gsap.to(this.mainThreeJS.mainLight.position, {
        x: -11,
        duration: 2,
        ease: "ease.in",
      })
    );
  }

  initThreeJS() {
    this.mainThreeJS = new MainThreeJS(this, { el: "ddo-model-canvas" });
    this.mainThreeJS.init();
  }

  // utility

  // app loader
  updateLoader(percentage) {
    const offset = (this.loaderCircumference * (100 - percentage)) / 100;
    this.loaderPercentage.textContent = `${Math.floor(percentage)}%`;
    this.loaderPath.style["stroke-dashoffset"] = offset;
  }

  // locomotive
  resetLocoScroll() {
    this.locoScroll.destroy();

    this.locoScroll = new LocomotiveScroll({
      el: document.querySelector(".smooth-scroll"),
      smooth: true,
      lerp: 0.1,
      touchMultiplier: 3,
      smartphone: {
        smooth: true,
      },
      tablet: {
        smooth: true,
      },
    });

    // each time Locomotive Scroll updates, tell ScrollTrigger to update too (sync positioning)
    this.locoScroll.on("scroll", ScrollTrigger.update);
  }

  createOrGetDiscoverScene(projectId) {
    if (this.discoverScenes[projectId]) return this.discoverScenes[projectId];

    //Discover animations
    var discoverDisplayTl = gsap.timeline({
      paused: true,
      onComplete: () => {
        let elem = document.querySelector(".discover-wrapper[data-id='" + projectId + "']");
        elem.setAttribute("aria-hidden", "false");

        this.discoverLocoScroll = new LocomotiveScroll({
          el: elem,
          smooth: true,
          lerp: 0.1,
          touchMultiplier: 3,
          smartphone: {
            smooth: true,
          },
          tablet: {
            smooth: true,
          },
        });
      },
      onReverseComplete: () => {
        let elem = document.querySelector(".discover-wrapper[data-id='" + projectId + "']");
        elem.setAttribute("aria-hidden", "false");
        document.getElementById("nav-about").style.display = "flex";
        this.discoverLocoScroll.destroy();
      },
    });
    discoverDisplayTl.set(".discover-wrapper[data-id='" + projectId + "']", {
      visibility: "visible",
    });
    discoverDisplayTl.to(
      "#home-wrapper",
      {
        opacity: 0,
        duration: 0.25,
        ease: "power2.in",
      },
      "<0"
    );
    discoverDisplayTl.to(
      ".ddo-backtext",
      {
        opacity: 0,
        duration: 0.5,
        ease: "power2.in",
      },
      "<0"
    );
    discoverDisplayTl.to(
      ".discover-wrapper[data-id='" + projectId + "']",
      {
        opacity: 1,
        y: 0,
        duration: 0.5,
        ease: "power2.in",
      },
      "<0"
    );
    discoverDisplayTl.to(
      this.mainThreeJS.object.position,
      {
        x: this.mainThreeJS.modelConf.right.low.pos.x,
        y: this.mainThreeJS.modelConf.right.low.pos.y,
        duration: 0.5,
        ease: "power2.out",
      },
      "<0"
    );

    this.discoverScenes[projectId] = discoverDisplayTl;

    return discoverDisplayTl;
  }

  // scenes / themes methods
  goToHomeScene() {
    if (this.currentScene == "home") {
      if (this.discoverActive) {
        this.mainThreeJS.triggerParticlesAccel();

        this.createOrGetDiscoverScene(this.discoverActive).reverse();

        this.discoverActive = false;

        this.locoScroll.start();
      }
      if (this.locoScroll.scroll.instance.scroll.y > 0) this.locoScroll.scrollTo("top");

      return;
    }

    let call = (direct) => {
      let waitTime = direct ? 0 : 150;
      setTimeout(() => {
        this.animations.about.forEach((tween) => tween.pause(0).kill(true));
        ScrollTrigger.getAll().forEach((t) => t.kill(true));

        this.goDark();

        this.mainThreeJS.object.positionName = "right";

        let aboutMeWrapper = document.getElementById("aboutme-wrapper");
        let homeWrapper = document.getElementById("home-wrapper");
        aboutMeWrapper.style.display = "none";
        homeWrapper.style.display = "block";
        aboutMeWrapper.setAttribute("aria-hidden", "true");
        homeWrapper.setAttribute("aria-hidden", "false");

        this.resetLocoScroll(); //Reset locomotive scroll instance

        gsap.to("#aboutme-wrapper", { opacity: 0, duration: 1 });

        document.getElementById("nav-home").style.display = "none";
        document.getElementById("nav-about").style.display = "flex";

        ScrollTrigger.refresh();

        if (this.aboutSceneTl) this.aboutSceneTl.pause();

        this._initHomeThreeJSAnimations();
        this._initHomeDOMAnimations();

        this.currentScene = "home";
      }, waitTime);
    };

    if (this.locoScroll.scroll.instance.scroll.y > 0) this.locoScroll.scrollTo("top", { callback: call });
    else call(true);
  }

  goToAboutScene() {
    if (this.currentScene == "about") return;

    let call = (direct) => {
      let waitTime = direct ? 0 : 150;
      setTimeout(() => {
        this.animations.home.forEach((tween) => tween.pause(0).kill(true));
        ScrollTrigger.getAll().forEach((t) => t.kill(true));

        this.goLight();

        this.mainThreeJS.object.positionName = "left";

        let aboutMeWrapper = document.getElementById("aboutme-wrapper");
        let homeWrapper = document.getElementById("home-wrapper");
        aboutMeWrapper.style.display = "block";
        homeWrapper.style.display = "none";
        aboutMeWrapper.setAttribute("aria-hidden", "false");
        homeWrapper.setAttribute("aria-hidden", "true");

        this.resetLocoScroll(); //Reset locomotive scroll instance

        gsap.to("#aboutme-wrapper", { opacity: 1, duration: 1 });

        document.getElementById("nav-about").style.display = "none";
        document.getElementById("nav-home").style.display = "flex";

        ScrollTrigger.refresh();

        if (this.homeSceneTl) this.homeSceneTl.pause();

        this._initAboutThreeJSAnimations();
        this._initAboutDOMAnimations();

        this.currentScene = "about";
      }, waitTime);
    };

    if (this.locoScroll.scroll.instance.scroll.y > 0) this.locoScroll.scrollTo("top", { callback: call });
    else call(true);
  }

  goDark() {
    if (this.currentTheme == "dark") return;
    else this.currentTheme = "dark";

    this.mainThreeJS.triggerParticlesAccel();

    //Brand logo
    gsap.to(".home-scene-button", {
      color: this.tailwindConf.colors.white,
      duration: 1,
      ease: "ease.in",
    });
    gsap.to(".home-scene-button path", {
      fill: this.tailwindConf.colors.white,
      duration: 1,
      ease: "ease.in",
    });
    //Cursor
    gsap.to("#cursor-circle svg circle", {
      stroke: this.tailwindConf.colors.primary,
      duration: 1,
      ease: "elastic.in",
    });
    gsap.to("#cursor-pointer svg circle", {
      fill: this.tailwindConf.colors.primary,
      duration: 0.5,
      ease: "elastic.in",
    });
    //Body
    gsap.to("body", {
      backgroundColor: this.tailwindConf.colors.background,
      duration: 1,
      ease: "ease.in",
    });
    //Fog color
    gsap.to(this.mainThreeJS.scene.fog.color, {
      r: 0,
      g: 0,
      b: 0,
    });
    //DDO Backtext
    gsap.to(".ddo-backtext", {
      color: "#000",
      duration: 1,
      ease: "ease.in",
    });
    //Particles material
    gsap.to(this.mainThreeJS.starMaterial.color, {
      r: 255,
      g: 255,
      b: 255,
    });
  }

  goLight() {
    if (this.currentTheme == "light") return;
    else this.currentTheme = "light";

    this.mainThreeJS.triggerParticlesAccel();

    //Brand logo
    gsap.to(".home-scene-button", {
      color: this.tailwindConf.colors.brown,
      duration: 1,
      ease: "ease.in",
    });
    gsap.to(".home-scene-button path", {
      fill: this.tailwindConf.colors.brown,
      duration: 1,
      ease: "ease.in",
    });
    //Cursor
    gsap.to("#cursor-circle svg circle", {
      stroke: this.tailwindConf.colors.brown,
      duration: 1,
      ease: "elastic.in",
    });
    gsap.to("#cursor-pointer svg circle", {
      fill: this.tailwindConf.colors.brown,
      duration: 0.5,
      ease: "elastic.in",
    });
    //Body
    gsap.to("body", {
      backgroundColor: this.tailwindConf.colors.primary,
      duration: 1,
      ease: "ease.in",
    });
    //Fog color
    gsap.to(this.mainThreeJS.scene.fog.color, {
      r: 1,
      g: 1,
      b: 1,
    });
    //DDO Backtext
    gsap.to(".ddo-backtext", {
      color: this.tailwindConf.colors["primary-dark"],
      duration: 1,
      ease: "ease.in",
    });
    //Particles material
    gsap.to(this.mainThreeJS.starMaterial.color, {
      r: 0,
      g: 0,
      b: 0,
    });
  }

  copyToClipboard(str) {
    /* ——— Derived from: https://hackernoon.com/copying-text-to-clipboard-with-javascript-df4d4988697f
           improved to add iOS device compatibility——— */
    const el = document.createElement("textarea"); // Create a <textarea> element

    let storeContentEditable = el.contentEditable;
    let storeReadOnly = el.readOnly;

    el.value = str; // Set its value to the string that you want copied
    el.contentEditable = true;
    el.readOnly = false;
    el.setAttribute("readonly", false); // Make it readonly false for iOS compatability
    el.setAttribute("contenteditable", true); // Make it editable for iOS
    el.style.position = "absolute";
    el.style.left = "-9999px"; // Move outside the screen to make it invisible
    document.body.appendChild(el); // Append the <textarea> element to the HTML document
    const selected =
      document.getSelection().rangeCount > 0 // Check if there is any content selected previously
        ? document.getSelection().getRangeAt(0) // Store selection if found
        : false; // Mark as false to know no selection existed before
    el.select(); // Select the <textarea> content
    el.setSelectionRange(0, 999999);
    document.execCommand("copy"); // Copy - only works as a result of a user action (e.g. click events)
    document.body.removeChild(el); // Remove the <textarea> element
    if (selected) {
      // If a selection existed before copying
      document.getSelection().removeAllRanges(); // Unselect everything on the HTML document
      document.getSelection().addRange(selected); // Restore the original selection
    }

    el.contentEditable = storeContentEditable;
    el.readOnly = storeReadOnly;
  }
}

let app = new Application();
app.initThreeJS();
