import * as THREE from "three";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader.js";

import modelGLBFile from "../assets/models/dylax_model.glb";
import modelMapFile from "../assets/textures/map.jpg";
import particleSpriteFile from "../assets/textures/star.png";

import { gsap } from "gsap";

export default class MainThreeJS {
  constructor(app, conf) {
    this.app = app;
    this.conf = {
      fov: 60,
      cameraX: 0,
      cameraY: 0,
      cameraZ: 120,
      cameraRotationX: 0,
      cameraRotationY: 0,
      cameraRotationZ: 0,

      mouseCoef: 0,

      modelX: 4,
      modelY: -8.3,
      modelZ: 110,
      modelRotationX: 0,
      modelRotationY: -80,
      modelRotationZ: 0,
      modelColor: 0xffc971, //0xfdfdaf #ffc971,
      modelRoughness: 0.5,
      modelMetalness: 1.1,
      scale: 24,

      movingLightsX: -10,
      movingLightsY: -10,
      movingLightsZ: 60,
      movingLightsIntensity: 1,

      //background: 0x181212, // #FFC971 0x2D2D2D,
      ambientColor: 0x251c3c, // #FFC971 #fdfdaf #181212
      ambientLightIntensity: 0.52,
      lightIntensity: 0.5,
      light1Color: 0x340077, // #ff8500 #E2711D 0x980AF1,
      light2Color: 0x490000, // #ff5400 0x66B6EC,
      light3Color: 0x947, // #ff7900 0xffffff,

      mainLightColor: 0xff7100,
      mainLightIntensity: 0.1,
      mainLightX: 6, //mainLightX: -25,
      mainLightY: -8, //mainLightY: 1,
      mainLightZ: 125, //mainLightZ: 10,

      secondLightColor: 0xff7100,
      secondLightIntensity: 18,
      secondLightX: 6, //mainLightX: -25,
      secondLightY: 3, //mainLightY: 1,
      secondLightZ: 130, //mainLightZ: 10,

      cursorLightColor: 0x1a02b2, //0x5330e2,
      cursorLightIntensity: 1.5,
      cursorLightZ: 112, //112,
      cursorLightOffsetX: 3,
      cursorLightOffsetY: -3,

      exposure: 1,
      bloomStrength: 3,
      bloomThreshold: 0,
      bloomRadius: 2,

      ...conf,
    };

    this.modelConf = {
      right: {
        regular: {
          pos: {
            x: 4,
            y: -8.4,
            z: 110,
          },
          rotation: {
            x: 0,
            y: -80,
            z: 0,
          },
          angleBounds: [-1.6, -1.13],
        },
        low: {
          pos: {
            x: 8,
            y: -10,
            z: 110,
          },
          rotation: {
            x: 0,
            y: -80,
            z: 0,
          },
          angleBounds: [-1.6, -1.13],
        },
      },
      left: {
        regular: {
          pos: {
            x: -4.5,
            y: -8.2,
            z: 110,
          },
          rotation: {
            x: 0,
            y: 70,
            z: 0,
          },
          angleBounds: [0.99, 1.4],
        },
        low: {
          pos: {
            x: -10,
            y: -10,
            z: 110,
          },
          rotation: {
            x: 0,
            y: 70,
            z: 0,
          },
          angleBounds: [0.99, 1.4],
        },
      },
    };

    this.myGLTFLoader = new GLTFLoader();

    this.mouse = {};
  }

  init() {
    this._initStats();

    this._initRenderer();
    this._initCamera();
    this._initResizeEvent();
    this._initScene();
    this._initLoadingManager();

    this._animate();
  }

  // init methods

  _initStats() {
    //this.stats = new Stats();
    //this.stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
    //document.body.appendChild(this.stats.dom);
  }

  _initLoadingManager() {
    THREE.DefaultLoadingManager.onLoad = () => {
      let loadedTimeline = new gsap.timeline({
        onComplete: () => {
          this.app.initAfterLoad();
        },
      });
      loadedTimeline.add(
        loadedTimeline.to(".app-loader-progress", {
          opacity: "0",
          duration: 0.25,
          ease: "ease.in",
        })
      );
      loadedTimeline.add(
        loadedTimeline.fromTo(
          ".ldr-dd-logo",
          {
            x: 45,
          },
          {
            x: 0,
            opacity: "1",
            duration: 0.5,
            ease: "ease.in",
          }
        )
      );
      loadedTimeline.add(
        loadedTimeline.fromTo(
          ".ldr-dd-logo-text",
          {
            x: -15,
          },
          {
            x: 0,
            opacity: "1",
            duration: 0.75,
            ease: "ease.in",
          }
        )
      );
      loadedTimeline.add(
        loadedTimeline.to(".app-loader", {
          opacity: 0,
          duration: 0.5,
          ease: "ease.out",
        })
      );
      loadedTimeline.add(
        loadedTimeline.to(
          ".ddo-model-canvas",
          {
            opacity: 1,
            duration: 0.5,
            ease: "ease.in",
          },
          ">-0.5"
        )
      );
      loadedTimeline.add(
        loadedTimeline.fromTo(
          "#cursor-circle",
          {
            scale: 0,
          },
          {
            scale: 1,
            opacity: 1,
            duration: 1,
          }
        )
      );
      loadedTimeline.add(
        loadedTimeline.to(".app-main", {
          display: "block",
        })
      );
      loadedTimeline.add(
        loadedTimeline.set(".app-loader", {
          display: "none",
        })
      );
    };

    THREE.DefaultLoadingManager.onProgress = (url, itemsLoaded, itemsTotal) => {
      this.app.updateLoader(Math.min(100, (itemsLoaded * 100) / itemsTotal));
    };
  }

  _initRenderer() {
    this.renderer = new THREE.WebGLRenderer({
      canvas: document.getElementById(this.conf.el),
      antialias: true,
      alpha: true,
    });

    this.renderer.toneMapping = THREE.CineonToneMapping; //THREE.ACESFilmicToneMapping;
    this.renderer.outputEncoding = THREE.sRGBEncoding;
  }

  _initCamera() {
    this.camera = new THREE.PerspectiveCamera(this.conf.fov);
    this.camera.position.set(this.conf.cameraX, this.conf.cameraY, this.conf.cameraZ);
    this.camera.rotation.set(THREE.Math.degToRad(this.conf.cameraRotationX), THREE.Math.degToRad(this.conf.cameraRotationY), THREE.Math.degToRad(this.conf.cameraRotationZ));
  }

  _initResizeEvent() {
    this.getRendererSize = () => {
      const cam = new THREE.PerspectiveCamera(this.camera.fov, this.camera.aspect);
      const vFOV = (cam.fov * Math.PI) / 180;
      const height = 2 * Math.tan(vFOV / 2) * Math.abs(this.conf.cameraZ);
      const width = height * cam.aspect;
      return [width, height];
    };
    let resizeCanvasToDisplaySize = () => {
      const canvas = this.renderer.domElement;
      // look up the size the canvas is being displayed
      let clientWidth = canvas.clientWidth;
      let clientHeight = canvas.clientHeight;

      // adjust displayBuffer size to match
      if (canvas.width !== clientWidth || canvas.height !== clientHeight) {
        // you must pass false here or three.js sadly fights the browser
        this.renderer.setSize(clientWidth, clientHeight, false);
        this.camera.aspect = clientWidth / clientHeight;
        this.camera.updateProjectionMatrix();

        // update any render target sizes here
        //const wsize = this.getRendererSize();
        //this.wWidth = wsize[0];
        //this.wHeight = wsize[1];
      }
    };

    resizeCanvasToDisplaySize();
    window.addEventListener("resize", resizeCanvasToDisplaySize, false);
  }

  _initScene() {
    this.scene = new THREE.Scene();
    if (this.conf.background) this.scene.background = new THREE.Color(this.conf.background);

    this.scene.fog = new THREE.Fog(0x000000, 0.5, 230); //-60 240

    this._initLights();
    this._initModel();
    this._initParticles();
  }

  _initLights() {
    //AMBIENT LIGHT
    this.ambientLight = new THREE.AmbientLight(this.conf.ambientColor);
    this.ambientLight.intensity = this.conf.ambientLightIntensity;
    this.scene.add(this.ambientLight);

    const lightDistance = 200;

    //LIGHT1
    this.light1 = new THREE.PointLight(this.conf.light1Color, this.conf.lightIntensity, lightDistance);
    this.light1.position.set(this.conf.movingLightsX, this.conf.movingLightsY, this.conf.movingLightsZ);
    this.scene.add(this.light1);

    //LIGHT2
    this.light2 = new THREE.PointLight(this.conf.light2Color, this.conf.lightIntensity, lightDistance);
    this.light2.position.set(this.conf.movingLightsX, this.conf.movingLightsY, this.conf.movingLightsZ);
    this.scene.add(this.light2);

    //LIGHT3
    this.light3 = new THREE.PointLight(this.conf.light3Color, this.conf.lightIntensity, lightDistance);
    this.light3.position.set(this.conf.movingLightsX, this.conf.movingLightsY, this.conf.movingLightsZ);
    this.scene.add(this.light3);

    //MAINLIGHT
    this.mainLight = new THREE.DirectionalLight(this.conf.mainLightColor, this.conf.mainLightIntensity);
    this.mainLight.position.set(this.conf.mainLightX, this.conf.mainLightY, this.conf.mainLightZ);
    this.scene.add(this.mainLight);

    //SECONDLIGHT
    this.secondLight = new THREE.SpotLight(this.conf.secondLightColor, this.conf.secondLightIntensity, 50);
    this.secondLight.position.set(this.conf.secondLightX, this.conf.secondLightY, this.conf.secondLightZ);
    this.scene.add(this.secondLight);

    //CURSOR LIGHT
    this.cursorLight = new THREE.DirectionalLight(this.conf.cursorLightColor); //#ff00ff #9095bf #191970
    this.cursorLight.intensity = 1.0;
    this.cursorLight.position.set(0, 0, 0);
    this.scene.add(this.cursorLight);

    this.cursorLight.nextPos = new THREE.Vector3(0, 0, 0);

    let computeCursorLightNextPos = (event) => {
      // Update the mouse variable
      event.preventDefault();
      //this.mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
      //this.mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
      this.mouse.x = -(event.clientX / window.innerWidth) * 2 + 1;
      this.mouse.y = (event.clientY / window.innerHeight) * 2 - 1;

      // Compute light position related to mouse pos
      var vector = new THREE.Vector3(this.mouse.x, this.mouse.y, this.conf.cursorLightZ);
      vector.unproject(this.camera);
      var dir = vector.sub(this.camera.position).normalize();
      var distance = 10;
      this.cursorLight.nextPos = this.camera.position.clone().add(dir.multiplyScalar(distance));
    };

    // Update on mouse move
    document.addEventListener("mousemove", computeCursorLightNextPos, false);

    // Move cursor light smoothly
    var speed = 0.05;
    var pos = { x: window.innerWidth / 2, y: window.innerHeight / 2 };
    var fpms = 60 / 1000;

    gsap.ticker.add((time, deltaTime) => {
      var delta = deltaTime * fpms;
      var dt = 1.0 - Math.pow(1.0 - speed, delta);

      pos.x += (this.cursorLight.nextPos.x - pos.x) * dt;
      pos.y += (this.cursorLight.nextPos.y - pos.y) * dt;
      this.cursorLight.position.copy(new THREE.Vector3(pos.x + this.conf.cursorLightOffsetX, pos.y + this.conf.cursorLightOffsetY, this.conf.cursorLightZ));
    });
  }

  _initModel() {
    this.myGLTFLoader.load(
      modelGLBFile,
      (gltf) => {
        this.object = gltf.scene.children[0];
        this.object.scale.set(this.conf.scale, this.conf.scale, this.conf.scale);
        this.object.position.set(9, -10, 120);
        this.object.rotation.set(THREE.Math.degToRad(this.conf.modelRotationX), THREE.Math.degToRad(this.conf.modelRotationY), THREE.Math.degToRad(this.conf.modelRotationZ));

        //Object screen position mode
        this.object.positionName = "right";

        //Mouse rotation bounds (radians)
        this.object.angleBounds = {
          right: [-1.6, -1.13],
          left: [0.99, 1.4],
        };

        //Get rotation speed (0 if out of bounds, and slowly reduced when approaching bounds)
        this.object.getRotationSpeed = (clockwise) => {
          let canRotate = clockwise ? this.object.rotation.y > this.object.angleBounds[this.object.positionName][0] : this.object.rotation.y < this.object.angleBounds[this.object.positionName][1];
          if (canRotate) {
            let baseSpeed = 0.001;
            let distanceThreshold = 0.15; //distance where it starts to slow down
            let slowCoef = 1; //slow down rate
            let distanceFromBound = Math.abs(this.object.rotation.y - this.object.angleBounds[this.object.positionName][clockwise ? 0 : 1]); //distance before reaching bound
            let speed = distanceFromBound < distanceThreshold ? (distanceFromBound * baseSpeed * slowCoef) / distanceThreshold : baseSpeed;

            return speed;
          } else {
            return 0;
          }
        };

        let material = new THREE.MeshStandardMaterial({
          color: this.conf.modelColor, //#888888
          roughness: this.conf.modelRoughness,
          metalness: this.conf.modelMetalness,
          envMapIntensity: 1.0,
        });
        new THREE.TextureLoader().load(modelMapFile, function (texture) {
          texture.encoding = THREE.sRGBEncoding;

          let pngCubeRenderTarget = pmremGenerator.fromEquirectangular(texture);

          material.envMap = pngCubeRenderTarget ? pngCubeRenderTarget.texture : null;
          material.needsUpdate = true;
          material.map = pngCubeRenderTarget ? pngCubeRenderTarget.texture : null;
          material.needsUpdate = true;

          texture.dispose();
        });

        var pmremGenerator = new THREE.PMREMGenerator(this.renderer);
        pmremGenerator.compileEquirectangularShader();

        this.object.material = material;

        this.scene.add(this.object);

        //SET LIGHTS TARGET
        this.mainLight.target = this.object;
        this.secondLight.target = this.object;

        this.cursorLight.target = this.object;
      },
      undefined,
      function (error) {
        console.error(error);
      }
    );
  }

  _initParticles() {
    this.starGeo = new THREE.Geometry();
    for (let i = 0; i < 6000; i++) {
      let star = new THREE.Vector3(Math.random() * 300 - 150, Math.random() * 300 - 70, Math.random() * 300 - 60);
      star.velocity = 0;
      star.acceleration = 0.0001 * (Math.random() + 1);
      this.starGeo.vertices.push(star);
    }
    let sprite = new THREE.TextureLoader().load(particleSpriteFile);
    this.starMaterial = new THREE.PointsMaterial({
      size: 0.2,
      //sizeAttenuation: true,
      map: sprite,
      transparent: true,
      //blending: THREE.AdditiveBlending,
    });
    this.starMaterial.color.setHSL(1, 1, 1);

    this.starGeo.accelMultiplier = 1;

    this.stars = new THREE.Points(this.starGeo, this.starMaterial);
    this.scene.add(this.stars);
  }

  // render methods

  _animate() {
    //this.stats.begin();

    this.time = Date.now() * 0.001;

    this._animateParticles();
    this._animateLights();
    this._animateModel();

    //Mobile camera zooming out
    if (window.innerWidth <= 640 && this.mobile != true) {
      this.mobile = true;
      this.camera.zoom = 1.4;
      this.camera.position.y = 1.4;
      this.camera.fov = 40;

      this.camera.updateProjectionMatrix();
    } else if (window.innerWidth > 640 && this.mobile == true) {
      this.mobile = false;
      this.camera.zoom = 1;
      this.camera.position.y = 0;
      this.camera.fov = this.conf.fov;

      this.camera.updateProjectionMatrix();
    }

    //this.stats.end();

    //setTimeout(() => {
    requestAnimationFrame(() => {
      this._animate();
    });
    //}, 1000 / 60);

    this.renderer.render(this.scene, this.camera);
  }

  // utility

  triggerParticlesAccel() {
    let starAccelTl = gsap.timeline();
    starAccelTl.fromTo(
      this.starGeo,
      {
        accelMultiplier: 10,
      },
      {
        accelMultiplier: 1,
        duration: 0.8,
        ease: "power2.inOut",
        delay: 0.6,
      }
    );
  }

  _animateParticles() {
    this.starGeo.vertices.forEach((p) => {
      p.velocity += p.acceleration;
      p.z += p.velocity * this.starGeo.accelMultiplier;
      if (p.z > 150) {
        p.z = -50;
        p.velocity = 0;
      }
    });
    this.starGeo.verticesNeedUpdate = true;
  }

  _animateLights() {
    //Animate colored lights position
    this.light1.position.x = Math.sin(this.time * 0.1) * this.conf.movingLightsX;
    this.light1.position.y = Math.cos(this.time * 0.2) * this.conf.movingLightsY;
    this.light2.position.x = Math.cos(this.time * 0.3) * this.conf.movingLightsX;
    this.light2.position.y = Math.sin(this.time * 0.4) * this.conf.movingLightsY;
    this.light3.position.x = Math.sin(this.time * 0.5) * this.conf.movingLightsX;
    this.light3.position.y = Math.sin(this.time * 0.6) * this.conf.movingLightsY;
  }

  _animateModel() {
    if (this.mobile) return;

    //Model rotation based on mouse X movement
    if (this.object && this.app.mouseOffset.x) {
      let speed = this.object.getRotationSpeed(this.app.mouseOffset.x < 0);
      if (speed > 0) {
        this.object.rotation.y += this.app.mouseOffset.x * speed;
      }
    }
  }
}
