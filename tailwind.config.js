module.exports = {
  theme: {
    purge: false,
    screens: {
      sm: "640px",
      md: "768px",
      lg: "992px",
      xl: "1280px",
    },
    colors: {
      transparent: "transparent",
      black: "#000",
      beige: "#EAE0C4",
      white: "#FFF",
      dark: "#1B142E",
      background: "#251c3c",
      brown: "#381E1E",
      "primary-light": "#FFF3DD",
      "primary-dark": "#b79c60",
      primary: "#FFCF74", //"#FFD984",
    },
    fill: (theme) => ({
      current: "currentColor",
      primary: theme("colors.primary"),
      dark: theme("colors.dark"),
    }),
    stroke: (theme) => ({
      current: "currentColor",
      primary: theme("colors.primary"),
      dark: theme("colors.dark"),
      brown: theme("colors.brown"),
    }),
    fontWeight: {
      normal: 400,
      medium: 500,
      bold: 700,
      black: 900,
    },
    zIndex: {
      0: 0,
      10: 10,
      15: 15,
      20: 20,
      30: 30,
      40: 40,
      50: 50,
      25: 25,
      50: 50,
      75: 75,
      100: 100,
      auto: "auto",
    },
    opacity: {
      0: "0",
      10: ".1",
      20: ".2",
      30: ".3",
      40: ".4",
      50: ".5",
      60: ".6",
      70: ".7",
      80: ".8",
      90: ".9",
      100: "1",
    },
    scale: {
      0: "0",
      25: ".25",
      50: ".5",
      75: ".75",
      90: ".9",
      95: ".95",
      100: "1",
      105: "1.05",
      110: "1.1",
      125: "1.25",
      150: "1.5",
      175: "1.75",
      200: "2",
    },
    cursor: {
      auto: "auto",
      default: "default",
      pointer: "pointer",
      "not-allowed": "not-allowed",
      none: "none",
    },
    inset: {
      0: 0,
      "2xl": "8rem",
      auto: "auto",
      "1/2": "50%",
      "1/4": "25%",
    },
    fontFamily: {
      sans: ["DM Sans", "sans_serif"],
      serif: ["Maragsa Display", "serif"],
    },
    fontSize: {
      tiniest: "0.7rem",
      tiny: "1rem",
      sm: "1.2rem",
      base: "1.5rem",
      md: "2rem",
      lg: "3.4rem",
      "2lg": "4.4rem",
      xl: "6rem",
      "2xl": "8rem",
      "3xl": "10rem",
      "4xl": "18rem",
      "5xl": "30rem",
    },
    container: {
      center: true,
    },

    // // Animations
    // animations: {
    //   slide: {
    //     from: {
    //       transform: "translate(0,0)",
    //     },
    //     to: {
    //       transform: "translate(-100%,0)",
    //     },
    //   },
    //   spin: {
    //     from: {
    //       transform: "rotate(0deg)",
    //     },
    //     to: {
    //       transform: "rotate(360deg)",
    //     },
    //   },
    // },
    // animationDuration: {
    //   default: "1s",
    //   "0s": "0s",
    //   "1s": "1s",
    //   "2s": "2s",
    //   "30s": "30s",
    // },

    // // Transitions
    // willChange: {
    //   // defaults to these values
    //   auto: "auto",
    //   scroll: "scroll-position",
    //   contents: "contents",
    //   opacity: "opacity",
    //   transform: "transform",
    //   position: "top, left",
    // },

    transitionProperty: {
      // defaults to these values
      none: "none",
      all: "all",
      color: "color",
      bg: "background-color",
      border: "border-color",
      colors: ["color", "background-color", "border-color"],
      opacity: "opacity",
      transform: "transform",
      player: ["width", "height", "clip-path", "background-color"],
      fill: ["fill"],
    },

    // // Filters
    // filter: {
    //   // defaults to {}
    //   none: "none",
    //   grayscale: "grayscale(1)",
    //   contrast: "contrast(1)",
    //   invert: "invert(1)",
    //   sepia: "sepia(1)",
    //   "grayscale-invert": "grayscale(1) invert(1)",
    // },

    extend: {
      // height: {
      //   // "1px": "1px", //h-1px
      //   // "2px": "2px",
      //   // "4px": "4px",
      //   // "6px": "6px",
      //   // "8px": "8px",
      //   // "120vh": "120vh",
      //   // "10rem": "10rem",
      //   // "15rem": "15rem",
      //   // "20rem": "20rem",
      // },
      // width: {
      //   "1px": "1px",
      //   "2px": "2px",
      //   "4px": "4px",
      //   "6px": "6px",
      //   "8px": "8px",
      //   "10rem": "10rem",
      //   "15rem": "15rem",
      //   "20rem": "20rem",
      // },
      // maxWidth: {
      //   "7xl": "88rem",
      //   "screen-sm": "64rem",
      //   "screen-md": "76.8rem",
      //   "screen-lg": "102.4rem",
      //   "screen-xl": "128rem",
      // },
      // cursor: {
      //   none: "none",
      //   grab: "grab",
      // },
      // transitionProperty: {
      //   size: ["width", "height"],
      //   transform: "transform",
      // },
      // spacing: {
      //   96: "18rem",
      //   128: "24rem",
      // },
    },
  },
  // gradientColorStops: (theme) => ({
  //   primary: "#3490dc",
  //   secondary: "#0B0F21",
  // }),
  variants: {
    accessibility: ["focus"],
    aspectRatio: ["responsive"],
    scale: ["hover"],
    background: ["hover", "focus", "group-hover"],
    backgroundOpacity: ["responsive", "hover", "focus", "group-hover"],
    opacity: ["hover", "focus", "group-hover"],
    textColor: ["responsive", "hover", "focus", "group-hover"],
  },
  plugins: [],
};
